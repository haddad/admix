The Audio Definition Model (ADM – [ITU-R. Recommendation BS.2076, EBU Tech 3364](https://www.itu.int/rec/R-REC-BS.2076/en)) is a metadata model that allows the format and content of audio files to be reliably described. Among other parameters, it allows to specify locations and individual gain values of sound sources, both of which support recording parameter changes over time.

The ADMix tool suite was developed for experimenting with reverberation tracks in ADM files, but they can also be used for more general ADM recording, playback and rendering. These tools currently only support a subset of the ADM specifications, but the most common features are already available.

The tools are compatible with the [Open Sound Control (OSC) protocol](https://en.wikipedia.org/wiki/Open_Sound_Control), which can be used to remote-control them from other applications, e.g. Digital Audio Workstations (DAWs).

![](https://forum.ircam.fr/media/uploads/Softwares/daw-diagram-499x500.png)

The software tools were developed within the European research project [ORPHEUS](https://orpheus-audio.eu/).

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 687645.

![](https://forum.ircam.fr/media/uploads/Softwares/adm-renderer-498x500.png) 

>**References**
>
> - [Matthias Geier](https://www.ircam.fr/person/matthias-geier/), [Thibaut Carpentier](https://www.ircam.fr/person/thibaut-carpentier/), [Markus Noisternig](https://www.ircam.fr/person/markus-noisternig/), and [Olivier Warusfel](https://www.ircam.fr/person/olivier-warusfel).
>
> - Software tools for object-based audio production using the audio definition model.
>
> - In Proc. of the [4th International Conference on Spatial Audio (ICSA)](https://hal.archives-ouvertes.fr/hal-01574183v1), Graz, Austria, Sept 2017.
>
>**Videos**
>
> - [Recording a static scene with ADMix Recorder](https://medias.ircam.fr/stream/int/video/files/2018/03/02/ADMix_Recorder_Static_Scene_HiQ.mp4.mp4)
>
> - [Recording a static scene with ADMix Recorder and LTC Timecode](https://medias.ircam.fr/stream/int/video/files/2018/03/02/ADMix_Recorder_LTC_HiQ.mp4.mp4)
>
> - [Recording a dynamic scene with ADMix Recorder](https://medias.ircam.fr/stream/int/video/files/2018/03/02/ADMix_Recorder_Dynamic_Scene_HiQ.mp4.mp4)